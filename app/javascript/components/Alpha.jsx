import React from 'react';
import Style from './alphaStyle.css';
import jsonData from './dict.json';

	    	



class Alpha extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    	bgColors: [['red','red','red','red'],
    				['red','red','red','red'],
    				['red','red','red', 'red'],
    				['red','red','red', 'red']
    			],
    	alphabets: [this.getAlphabets(),
    				this.getAlphabets(),
    				this.getAlphabets(),
    				this.getAlphabets()
    				],
    	selectedAlphabet: '',
    	position: [2,2],
    	score: 0,
    	test: false,
    	initial: true,
    	time: {}, 
    	seconds: 60,
    	prevAlphabhet: []
    	
    	 
		};
	this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  secondsToTime(secs){
    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      "h": hours,
      "m": minutes,
      "s": seconds
    };
    return obj;
  }

  componentDidMount() {
    let timeLeftVar = this.secondsToTime(this.state.seconds);
    this.setState({ time: timeLeftVar });
  }

  startTimer() {
    if (this.timer == 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 100);
    }
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });
    
    // Check if we're at zero.
    if (seconds == 0) { 
      clearInterval(this.timer);
    }
  }


getAlphabets(){
 	var str = 'abcdefghijklmnopqrstuvwxyz';
 	var alphabets = [];
 	for (var i = 0; i < 4;  i++ ){
 		alphabets.push(str[Math.floor(Math.random() * 26)]);
 	}
    return alphabets;                
}

isValid(position){
	if (this.state.initial){
		this.setState({
		    initial: false
		});
		return true;
	}
	console.log(position, this.state.position);
	if (Math.abs(this.state.position[0] - position[0]) > 1 || Math.abs(this.state.position[1] - position[1]) > 1){
		return false;
	}
	if (Math.abs(this.state.position[0] - position[0]) >= 1 && Math.abs(this.state.position[1] - position[1]) >= 1){
		return false;
	}
	return true;  
}


wordIsValid(word){
	
	var dictionary = JSON.parse(JSON.stringify(jsonData));
	
	return dictionary.includes(word) ;
	//return Dictionary.hasOwnProperty(word);
	
}

 
handleClick(e) {
	if (this.timer == 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
	var selection = this.state.selectedAlphabet;
	var colors = this.state.bgColors;
	var position = e.target.value.split(",").map(function(v){
		return parseInt(v);
	})
	selection = selection + this.state.alphabets[position[0]][position[1]];
	if (this.isValid(position)){
		colors[position[0]][position[1]] = 'blue';
	    this.setState({
	      bgColors: colors,
	      selectedAlphabet: selection,
	      position: position
	    });
	    if(this.wordIsValid(selection)){
	    	console.log("hurray");
	    	const prevAlphabhet = this.state.prevAlphabhet;

	    	
	    	if (prevAlphabhet.includes(selection)){

	    		this.setState((state) => {
       				prevAlphabhet: state.prevAlphabhet.push(selection);
   				});
	    	}
	    	else{
	    		this.setState((state) => {
       				prevAlphabhet: state.prevAlphabhet.push(selection);
   				});

	    		var score = this.state.score;
		    this.setState({
		      score: score + selection.length
		    });
			}

	    }
	    else{
	    	console.log("Go on")
	    }
	}
	else{
		colors[position[0]][position[1]] = 'blue';
		position = e.target.value.split(",").map(function(v){
		return parseInt(v);
		})
		selection = this.state.alphabets[position[0]][position[1]];
		this.setState({
	      bgColors: colors,
	      selectedAlphabet: selection,
	      position: position
	    });
	}
  
  }



 createTable = () => {
    let table = []
    var pos = 0;
    for (let i = 0; i < 4; i++) {
      let children = []
      for (let j = 0; j < 4; j++) {
        children.push(<td key={pos.toString()}>{<button  value={`${i},${j}`}  onClick={e => this.handleClick(e)}  style={{backgroundColor:this.state.bgColors[i][j]}} className = "button button1 " >{this.state.alphabets[i][j]}</button>
}</td>)
		pos +=1 ;
      }                     
      table.push(<tr key={pos.toString()}>{children}</tr>)
    }
    return table
  }


	
	render() {

	    return (

	    	<div className = "square container-fluid">
	    	<div><strong>click on  button to start Boggle game</strong><p>Find as many words in One minute.</p><h2>m: {this.state.time.m} s: {this.state.time.s} </h2></div>

	    	<span className= "score">Score: {this.state.score}</span>
	    	<div><strong>Words Found:</strong>{this.state.prevAlphabhet.join(",")}</div>
	    	<div>Selected Words:{this.state.selectedAlphabet}</div>
	    	<ul>
	    		<li>{this.state.selectedAlphabet}</li>
	    	</ul>
	    	
	    	
	    	
	    	
	    	

                   
	    <table className="tabrotate">
	    <thead></thead>
	    <tbody>
        {this.createTable()}
        </tbody>
      </table>
	    	

	     	</div>
	   );                             

	}
}

export default Alpha;